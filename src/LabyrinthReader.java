import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class LabyrinthReader {

    public static Labyrinth tryReadLabyrinth(String fileName) {
        try {
            File imageFile = new File(fileName);
            BufferedImage bufferedImage = ImageIO.read(imageFile);
            return getLabyrinth(bufferedImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Labyrinth getLabyrinth(BufferedImage bufferedImage) {
        int imageWidth = bufferedImage.getWidth();
        int imageHeight = bufferedImage.getHeight();

        Labyrinth labyrinth = new Labyrinth();
        ArrayList<Point> whites = new ArrayList<>();
        ArrayList<Point> oranges = new ArrayList<>();

        MapPoint[][] map = new MapPoint[imageWidth][imageHeight];
        for (int x = 0; x < imageWidth; x++) {
            for (int y = 0; y < imageHeight; y++) {
                int color = bufferedImage.getRGB(x, y);

                Point currentPoint = new Point(x, y);
                MapPoint mapPoint = new MapPoint();
                mapPoint.weight = Labyrinth.UNAVAILABLE_POINT;

                switch (color) {
                    case ColorRGB.ORANGE_RGB:
                        oranges.add(currentPoint);
                        break;
                    case ColorRGB.WHITE_RGB:
                        mapPoint.index = whites.size();
                        whites.add(currentPoint);
                        break;
                    case ColorRGB.GREEN_RGB:
                        mapPoint.index = whites.size();
                        labyrinth.startIndex = whites.size();
                        whites.add(currentPoint);
                        break;
                    case ColorRGB.RED_RGB:
                        mapPoint.index = whites.size();
                        labyrinth.finishIndex = whites.size();
                        whites.add(currentPoint);
                        break;
                }
                map[x][y] = mapPoint;
            }
        }

        labyrinth.oranges = oranges;
        labyrinth.whites = whites;
        labyrinth.map = map;
        return labyrinth;
    }
}

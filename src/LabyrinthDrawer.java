import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class LabyrinthDrawer {
    private static final String format = "png";
    private static final String fileExtension = "." + format;

    public static String drawPath(Labyrinth labyrinth, String filename) {
        String newFilename = filename + "_out";
        BufferedImage image = tryGetBufferedImage(filename);
        image = setPathToBufferedImage(labyrinth, image);
        if (tryWriteBufferedImageToFile(newFilename, image)) {
            return newFilename;
        } else {
            return null;
        }
    }

    private static BufferedImage setPathToBufferedImage(Labyrinth labyrinth, BufferedImage img) {
        int currentPointIndex = labyrinth.finishIndex;
        int pathLength = 0;
        while (
                labyrinth.parents[currentPointIndex] != labyrinth.startIndex &&
                        labyrinth.parents[currentPointIndex] != 0
                ) {
            pathLength++;
            currentPointIndex = labyrinth.parents[currentPointIndex];
            Point currentPoint = labyrinth.whites.get(currentPointIndex);
            img.setRGB(currentPoint.x, currentPoint.y, ColorRGB.GRAY_RGB);
        }
        System.out.println("Path length: " + pathLength);
        return img;
    }

    public static String drawRadiationMap(Labyrinth labyrinth, String filename) {
        String newFilename = filename + "_radiation";
        BufferedImage image = tryGetBufferedImage(filename);
        image = setRadiationToBufferedImage(labyrinth, image);
        if (tryWriteBufferedImageToFile(newFilename, image)) {
            return newFilename;
        } else {
            return null;
        }
    }

    private static BufferedImage setRadiationToBufferedImage(Labyrinth labyrinth, BufferedImage image) {
        Color[] rainbowColors = getRainbowColors();
        int radiationStep = (int) labyrinth.maxRadiation / rainbowColors.length;
        for (Point point : labyrinth.whites) {

            int tX = point.x;
            int tY = point.y;
            int colorIndex = (labyrinth.map[tX][tY].weight / radiationStep);
            colorIndex = (colorIndex >= rainbowColors.length) ? rainbowColors.length - 1 : colorIndex;

            Color color = rainbowColors[colorIndex];
            image.setRGB(tX, tY, color.getRGB());
        }
        return image;
    }

    private static BufferedImage tryGetBufferedImage(String filename) {
        try {
            return ImageIO.read(new File(filename + fileExtension));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static boolean tryWriteBufferedImageToFile(String filename, BufferedImage image) {
        try {
            return ImageIO.write(image, format, new File(filename + fileExtension));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private static Color[] getRainbowColors() {
        Color[] colors = new Color[1275];
        int counter = 0;

        //purple to blue
        for (int r = 255; r > 0; r--)
            colors[counter++] = new Color(r, 0, 255);
        //blue to light blue
        for (int g = 0; g < 255; g++)
            colors[counter++] = new Color(0, g, 255);
        //light blue to green
        for (int b = 255; b > 0; b--)
            colors[counter++] = new Color(0, 255, b);
        //green to yellow
        for (int r = 0; r < 255; r++)
            colors[counter++] = new Color(r, 255, 0);
        //yellow to red
        for (int g = 255; g > 0; g--)
            colors[counter++] = new Color(255, g, 0);

        return colors;
    }
}

public class ColorRGB {
    public static final int WHITE_RGB = -1;
    public static final int BLACK_RGB = -16777216;
    public static final int GRAY_RGB = 7895160;
    public static final int ORANGE_RGB = -32985;
    public static final int GREEN_RGB = -14503604;
    public static final int RED_RGB = -1237980;

}

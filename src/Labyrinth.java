import java.awt.*;
import java.awt.geom.Line2D;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Labyrinth {
    public static final byte UNAVAILABLE_POINT = -1;
    public static final int INFINITY_WEIGHT = Integer.MAX_VALUE;

    private final int[] QUEUE_ORDER = {0, -1, 1};
    private final int MAX_PATH_LENGTH = 1000;

    public MapPoint[][] map;
    public ArrayList<Point> whites;
    public ArrayList<Point> oranges;
    public int startIndex;
    public int finishIndex;
    public double maxRadiation = 0;
    public int[] parents;
    private int[] newParents;

    public void calcRadiation() {

        int count = 0;
        int whitesCount = whites.size();
        int currentProgress = 0;

        System.out.println("Calculation progress:");

        for (Point white : whites) {
            double radiation = 0;
            for (Point orange : oranges) {
                double distance = white.distance(orange);
                radiation += 100000.0 / (distance * distance);
//                radiation += 1000000.0 * Math.exp(-distance/2);
            }
            map[white.x][white.y].weight = (int) radiation;
            if (radiation > maxRadiation) maxRadiation = radiation;
            if ((count++ * 100 / whitesCount) > currentProgress) {
                currentProgress++;
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                System.out.println(currentProgress + "% " + sdf.format(cal.getTime()));
            }
        }
    }

    public void findPath() {
        double[] distances = initDistances();
        newParents = new int[whites.size()];
        parents = new int[whites.size()];

        for (int i = 1; i < whites.size(); i++) {
            boolean changed = false;
            for (int first = 0; first < whites.size(); first++) {
                if (distances[first] == INFINITY_WEIGHT) {
                    continue;
                }

                Point firstPoint = whites.get(first);
                ArrayList<Integer> adjacents = getAdjacentPoints(firstPoint);
                for (int second : adjacents) {
                    Point secondPoint = whites.get(second);
                    double newDistance = distances[first] + map[secondPoint.x][secondPoint.y].weight;
                    if (distances[second] > newDistance) {
                        distances[second] = newDistance;
                        newParents[second] = first;
                        changed = true;
                    }
                }
            }

            int currentPoint = finishIndex;
            int pathLength = 0;
            while (newParents[currentPoint] != startIndex && newParents[currentPoint] != 0) {
                pathLength += whites.get(currentPoint).distance(whites.get(newParents[currentPoint]));
                currentPoint = newParents[currentPoint];
            }
            if (pathLength >= MAX_PATH_LENGTH) {
                break;
            } else {
                System.arraycopy(newParents, 0, parents, 0, parents.length);
            }
            if (!changed)
                break;
            System.out.println("Iteration: " + i + "; Path length: " + pathLength + "; Radiation: " + distances[finishIndex]);
        }
        System.out.println("Path found!");
    }

    private double[] initDistances() {
        double[] distances = new double[whites.size()];
        for (int i = 0; i < whites.size(); i++) {
            distances[i] = INFINITY_WEIGHT;
        }
        distances[startIndex] = 0;
        return distances;
    }

    public ArrayList<Integer> getAdjacentPoints(Point point) {
        ArrayList<Integer> adjacentPoints = new ArrayList<>();
        for (int dx : QUEUE_ORDER) {
            for (int dy : QUEUE_ORDER) {
                if (dx == 0 && dy == 0) {
                    continue;
                }
                int x = dx + point.x;
                int y = dy + point.y;
                if (isIndexAdjacent(x, y, point))
                    adjacentPoints.add(map[x][y].index);
            }
        }
        return adjacentPoints;
    }

    private boolean isIndexAdjacent(int x, int y, Point point) {
        return (!isIndexOutOfRange(x, y) && isIndexAvailable(x, y) && isIndexReachableFromPoint(x, y, point));
    }

    private boolean isIndexOutOfRange(int x, int y) {
        return (x < 0 || y < 0 || x >= map.length || y >= map[0].length);
    }

    private boolean isIndexAvailable(int x, int y) {
        return (map[x][y].weight != UNAVAILABLE_POINT);
    }

    private boolean isIndexReachableFromPoint(int x, int y, Point point) {
        return !(map[x][point.y].weight == UNAVAILABLE_POINT || map[point.x][y].weight == UNAVAILABLE_POINT);
    }
}

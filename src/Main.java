public class Main {

    public static void main(String[] args) {
        String filename = "images/map";
        Labyrinth labyrinth = LabyrinthReader.tryReadLabyrinth(filename+".png");
        System.out.println("Reading complete! Whites: " + labyrinth.whites.size() + " Oranges: " + labyrinth.oranges.size());
        labyrinth.calcRadiation();

        labyrinth.findPath();
        String radiationFile = LabyrinthDrawer.drawRadiationMap(labyrinth, filename);
        LabyrinthDrawer.drawPath(labyrinth, radiationFile);
    }
}
